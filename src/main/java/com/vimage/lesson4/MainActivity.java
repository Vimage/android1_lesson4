package com.vimage.lesson4;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // !!! В ресурсы строки и константы пока не стал многие запихивать
    // при повороте не стал реализовывать сохранение выбранных значений пользователя (всё сбрасывается)


    CharSequence[] nChooseList = {"Change Name", "Delete button"};
    CharSequence[] nChooseColor = {"Red", "White", "Black", "Green"};
    Button currButton; // текущая выбранная кнока. //TODO это ж типа глобальной переменной, незнаю так стоит в яве делать или нет???
    //String choosedColor; //цвет бэкграунда, который выбрали
    int whichColor;// номер цвета бэкграунда, который выбрали

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button;
        button = (Button) findViewById(R.id.button11);
        assert button != null;
        registerForContextMenu(button); // на эту кнопку повесим контекстное меню

        // тупо всем кнопкам присвоим слушатель нажатия

        button = (Button) findViewById(R.id.button1);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button2);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button3);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button4);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button5);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button6);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button7);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button8);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button9);
        assert button != null;
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button10);
        assert button != null;
        button.setOnClickListener(this);





    }


    // тут сделаю слушатель нажатия на кнопки для вызова меню
    DialogInterface.OnClickListener dialogIntfListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            switch (i) {
                case 0:
                    createCustomCodeDialog(currButton);
                    //Toast.makeText(MainActivity.this, "0", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    deleteButton(currButton);
                    break;
                default:


                    //Toast.makeText(MainActivity.this, nChooseList[i], Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void deleteButton(Button currButton) {
        String buttonText = (String) currButton.getText();// найдём текст на кнопке
        LinearLayout layout = (LinearLayout) findViewById(R.id.layoutMain);
        layout.removeView(currButton);
        Toast.makeText(MainActivity.this, "Button '" + buttonText + "' has been deleted", Toast.LENGTH_SHORT).show();
    }


    public void createListDialog(View view) {

        String buttonText = (String) currButton.getText();// найдём текст на кнопке

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Choose action for button '" + buttonText + "'")
                .setItems(nChooseList, dialogIntfListener)
                .setNeutralButton("Cancel", null)
                .show();

    }


    @Override
    public void onClick(View view) {

        currButton = ((Button) view);// приведём к кнопке,установим текущую нажатую кнопку

        createListDialog(view);

    }

    // вызов нашего кастомного диалога на кнопке
    public void createCustomCodeDialog(View view) {

        final View viewForDialog = createViewForDialog(currButton.getText().toString());
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Enter New button name")
                .setView(viewForDialog)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Change name", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((LinearLayout) viewForDialog).getChildAt(0);//(EditText) viewForDialog.findViewById(123);
                        currButton.setText(editText.getText().toString());
                    }
                })
                .show();
    }

    // вьюшка для диалога указания имени кнопки
    private View createViewForDialog(String editingText) {
        LinearLayout linearLayout = new LinearLayout(MainActivity.this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParamsForView = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsForView.gravity = Gravity.CENTER_HORIZONTAL;

        EditText editText = new EditText(MainActivity.this);
        //editText.setId(123);
        editText.setText(editingText);
        editText.setTextSize(20);
        linearLayout.addView(editText, layoutParamsForView);
        return linearLayout;
    }

    // создадим меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.my_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        actionsByOptionsMenu(item);

        return super.onOptionsItemSelected(item);
    }

    private void actionsByOptionsMenu(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bgcolor:
                createColorDialog(findViewById(R.id.layoutMain));
                break;
            case R.id.menu_exit:
                createExitDialog();
                break;
            case R.id.menu_about:
                createAboutDialog();
                break;
        }
    }

    private void createExitDialog() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Exit Application")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNeutralButton("Help me to choose...", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        createHelpDialog();
                    }
                })
                .show();
    }

    private void createHelpDialog() {
        //data will not be saved when you exit the application
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Help for exit the application")
                .setMessage("Data will not be saved when you exit the application")
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    private void createAboutDialog() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Application about")
                .setMessage("This application is the test version for Android Lessons")
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }


    // диалог с радиобаттонами - для выбора цвета фона
    public void createColorDialog(final View view) {

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Choose background color")
                .setSingleChoiceItems(nChooseColor, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //choosedColor = ((String) nChooseColor[which]);
                        whichColor = which;
                    }
                })

                .setPositiveButton("Choose this", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int idColor = 0;
                        switch (whichColor) {
                            case 0:
                                idColor = R.color.red;
                                break;
                            case 1:
                                idColor = R.color.white;
                                break;
                            case 2:
                                idColor = R.color.black;
                                break;
                            case 3:
                                idColor = R.color.green;
                                break;

                            default:

                        }

                        view.setBackgroundColor(getResources().getColor(idColor));
                        Toast.makeText(MainActivity.this, "Your choice - " + (nChooseColor[whichColor]) + " color of background", Toast.LENGTH_LONG).show();
                    }
                })
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();

    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        actionsByOptionsMenu(item);

        return super.onContextItemSelected(item);
    }
}
